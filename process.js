const fetch = require('node-fetch');
const HttpsProxyAgent = require('https-proxy-agent');
const fs = require("fs");
const readline = require('readline');

main();

async function main() {
    const writeStream = fs.createWriteStream('./output.csv', {flags:'a'});

    const rl = readline.createInterface({
        input: fs.createReadStream('./proxies.txt'),
        output: process.stdout,
        terminal: false
    });

    for await (const proxy of rl) {
        const info = await getProxyInfoByGeoPlugin('http://'+proxy);
        const data = [
            proxy,
            info?.geoplugin_countryCode ?? '',
            info?.geoplugin_continentCode ?? '',
            info?.geoplugin_city ?? ''
        ];
        writeStream.write(data.join(',')+'\n');
        console.log(proxy, JSON.stringify(data));
    }
    writeStream.close();
}

/*
{
    "geoplugin_request":"113.163.234.110",
    "geoplugin_status":206,
    "geoplugin_delay":"2ms",
    "geoplugin_credit":"Some of the returned data includes GeoLite data created by MaxMind, available from <a href='http://www.maxmind.com'>http://www.maxmind.com</a>.",
    "geoplugin_city":"",
    "geoplugin_region":"",
    "geoplugin_regionCode":"",
    "geoplugin_regionName":"",
    "geoplugin_areaCode":"",
    "geoplugin_dmaCode":"",
    "geoplugin_countryCode":"VN",
    "geoplugin_countryName":"Vietnam",
    "geoplugin_inEU":0,
    "geoplugin_euVATrate":false,
    "geoplugin_continentCode":"AS",
    "geoplugin_continentName":"Asia",
    "geoplugin_latitude":"16.0016",
    "geoplugin_longitude":"105.9986",
    "geoplugin_locationAccuracyRadius":"50",
    "geoplugin_timezone":"Asia/Vientiane",
    "geoplugin_currencyCode":"VND",
    "geoplugin_currencySymbol":"₫",
    "geoplugin_currencySymbol_UTF8":"₫",
    "geoplugin_currencyConverter":23050
}
 */
async function getProxyInfoByGeoPlugin(proxy) {
    const options = {
        method: 'get',
        timeout: 10000,
        headers: {
            'Content-Type': 'application/json'
        }
    };

    try {
        const response = await __fetchViaProxy('http://www.geoplugin.net/json.gp', proxy, options);
        return await response.json();
    } catch (e) {
        console.log(e);
        return {error: e.toString()};
    }
}

async function __fetchViaProxy(url, proxy, options = {}) {
    return timeout(async () => {
        options.agent = new HttpsProxyAgent(proxy);
        return await fetch(url, options);
    }, options.timeout ? options.timeout : 10000);
}

function timeout(fn, timeout = 30000) {
    return Promise.race([
        fn(),
        new Promise((_, reject) =>
            setTimeout(() => reject(new Error('timeout')), timeout)
        )
    ]);
}